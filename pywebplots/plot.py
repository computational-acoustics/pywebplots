from pywebplots import config
import plotly.graph_objects as go
import plotly.subplots as su


def canvas(basic_config: config.BasicConfig) -> go.Figure:

    fig = go.Figure(layout=basic_config.layout)
    fig.update_layout(font={'family': basic_config.font_name})

    fig.update_xaxes(
        showline=True,
        zeroline=False,
        mirror=True,
        linecolor=basic_config.box_color,
        gridcolor=basic_config.grid_color,
        zerolinecolor=basic_config.axis_color

    )

    fig.update_yaxes(
        showline=True,
        zeroline=False,
        mirror=True,
        linecolor=basic_config.box_color,
        gridcolor=basic_config.grid_color,
        zerolinecolor=basic_config.axis_color
    )

    return fig


def subplots(basic_config: config.BasicConfig, **kwargs) -> go.Figure:

    fig = su.make_subplots(**kwargs)
    fig.update_layout(basic_config.layout)
    fig.update_layout(font={'family': basic_config.font_name})

    for r in range(kwargs['rows']):
        for c in range(kwargs['cols']):

            fig.update_xaxes(
                showline=True,
                zeroline=False,
                mirror=True,
                linecolor=basic_config.box_color,
                gridcolor=basic_config.grid_color,
                zerolinecolor=basic_config.axis_color,
                row=r,
                col=c
            )

            fig.update_yaxes(
                showline=True,
                zeroline=False,
                mirror=True,
                linecolor=basic_config.box_color,
                gridcolor=basic_config.grid_color,
                zerolinecolor=basic_config.axis_color,
                row=r,
                col=c
            )

    return fig


def add_line(fig: go.Figure, **kwargs) -> None:
    fig.add_trace(go.Scatter(**kwargs))


def add_histogram(fig: go.Figure, **kwargs) -> None:
    fig.add_trace(go.Histogram(**kwargs))


def add_bar(fig: go.Figure, **kwargs) -> None:
    fig.add_trace(go.Bar(**kwargs))


def add_heatmap(fig: go.Figure, **kwargs) -> None:
    fig.add_trace(go.Heatmap(**kwargs))


def add_isosurface(fig: go.Figure, **kwargs) -> None:
    fig.add_trace(go.Isosurface(**kwargs))


def add_line_subplot(fig: go.Figure, row: int, col: int, **kwargs) -> None:
    fig.add_trace(go.Scatter(**kwargs), row=row, col=col)
