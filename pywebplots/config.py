import dataclasses
import pathlib
import tomli
import typing
import plotly.graph_objects as go


def _make_default_layout() -> typing.Dict[str, str]:
    return {
        'paper_bgcolor': 'rgba(0,0,0,0)',
        'plot_bgcolor': 'rgba(0,0,0,0)'
    }


@dataclasses.dataclass
class BasicConfig:
    font_name: str = 'avenir'

    layout: typing.Dict[str, str] = dataclasses.field(default_factory=_make_default_layout)

    box_color: str = 'rgba(0,0,0,0)'
    grid_color: str = 'rgba(0,0,0,0.1)'
    axis_color: str = 'rgba(0,0,0,1)'

    def from_toml(self, conf_path: pathlib.Path) -> None:
        with open(conf_path, mode='rb') as cf:
            toml = tomli.load(cf)

        body_classes: str = toml['params']['body_classes']
        self.font_name = body_classes.split(' ')[0]
