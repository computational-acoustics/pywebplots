import plotly.graph_objects as go
import plotly
import pathlib


def save_plot(fig: go.Figure, dst: pathlib.Path) -> None:
    plotly.offline.plot(
        figure_or_data=fig,
        filename=dst.absolute().as_posix(),
        include_plotlyjs=False
    )


def save_plot_with_script(fig: go.Figure, dst: pathlib.Path) -> None:

    save_plot(fig=fig, dst=dst)

    with open(dst, 'r') as f:
        lines = f.readlines()

    lines_edit = (
        ['<script src="https://cdn.plot.ly/plotly-latest.min.js"></script>', ] +
        lines
    )

    with open(dst, 'w') as f:
        f.write('\n'.join(lines_edit))
